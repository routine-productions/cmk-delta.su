<div class="Advantage">
    <hr/>
    <span class="Advantage-Head">Философия компании</span>

    <p class="Advantages-Text">
        Мы всегда строим отношения со своими клиентами и партнерами на взаимовыгодной основе, развивая прочное и
        долговременное сотрудничество, работаем честно, уважаем права и интересы людей, организаций, с которыми
        сотрудничаем.
    </p>

    <hr/>

    <span class="Advantage-Head">Преимущества работы с нами</span>
    <div class="Advantage-Block">
        <div class="Wallet">
            <img src="/ico/wallet.png" alt=""/>
            <p>Гибкая ценовая политика</p>
        </div>
        <div class="User">
            <img src="/ico/user.png" alt=""/>
            <p>Квалифицированный<br> и аттестованный<br> персонал</p>
        </div>
        <div class="Factory">
            <img src="/ico/factory.png" alt=""/>
            <p>Собственные производственные<br> мощности и вспомогательное<br> производство</p>
        </div>
        <div class="Concrete">
            <img src="/ico/concrete.png" alt=""/>
            <p>Современное прикладное<br> оборудование и техника</p>
        </div>
    </div>

</div>
<div class="Advantage">
    <hr/>
    <span class="Advantage-Head">Руководители компании</span>

    <div class="Advantage-Block Advantage-Block-Tri">
        <div>
            <p class="Name">Зеленкин Сергей Александрович</p>
            <p class="Position">Генеральный директор ООО "СМК "ДЕЛЬТА"</p>
            <p class="Data">Приемная: телефон/факс (342) 256-56-93</p>
            <p class="Data">E-mail:
                <a href="mailto:s.zelenkin@cmkdelta.ru">s.zelenkin@cmkdelta.ru</a>
            </p>
        </div>
        <div>
            <p class="Name">Веселицкий Александр Вячеславович</p>
            <p class="Position">Первый заместитель генерального директора, главный инженер ООО "СМК "ДЕЛЬТА"</p>
            <p class="Data">Приемная: телефон/факс (342) 256-56-93</p>
            <p class="Data">E-mail:
                <a href="mailto:a.veselitskiy@cmkdelta.ru">a.veselitskiy@cmkdelta.ru</a>
            </p>

        </div>
        <div>
            <p class="Name">Королёва Льбовь Васильевна</p>
            <p class="Position">Заместитель генерального директора, главный бухгалтер ООО "СМК "ДЕЛЬТА"</p>
            <p class="Data">Приемная: телефон/факс (342) 256-56-93</p>
            <p class="Data">E-mail: <a href="mailto:l.koroleva@cmkdelta.ru  ">l.koroleva@cmkdelta.ru</a></p>
        </div>
    </div>
    <hr/>

</div>
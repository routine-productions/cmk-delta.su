<div class="Advantage">
    <hr/>
    <span class="Advantage-Head">СПЕЦИАЛЬНАЯ И ГРУЗОПОДЪЕМНАЯ ТЕХНИКА</span>

    <p class="Advantages-Text">
        Для выполнения заказа спец. техники воспользуйтесь телефонным номером +7&nbsp;(342)&nbsp;256-56-75 или отправьте
        электронное сообщение на адрес:
        <a href="mailto:snab@cmkdelta.ru">snab@cmkdelta.ru</a>
        .
    </p>

    <hr/>

    <div class="Advantage-Block">
        <div class="Wallet">
            <img src="/images/car_1.png" alt=""/>
            <p>КС 55717<br><span>Автомобильный кран<br>Грузопоъемность 25 т</span></p>
        </div>
        <div class="User">
            <img src="/images/car_2.png" alt=""/>
            <p>
                DAEWOO-NOVUS<br>
                <span>Автомобиль бортовой<br>
                    Длина кузова - 9 м<br>
                Грузоподъемность - 8 т</span></p>
        </div>
        <div class="Factory">
            <img src="/images/car_3.png" alt=""/>
            <p>
                TADANO<br> <span>Автомобильный кран на спец. шасси<br>Грузоподъемность - 63 т</span></p>
        </div>
        <div class="Concrete">
            <img src="/images/car_4.png" alt=""/>
            <p>
                МКГ 25 БР<br> <span>Гусеничный монтажный кран<br>Грузоподъемность - 25 т</span></p>
        </div>
    </div>
</div>

<div class="Our-Projects" id="projects">
    <p class="Our-Projects-Head">НАШИ ПРОЕКТЫ</p>

    <div class="Our-Projects-Parts">
        <div class="Our-Projects-Part">
            <div class="Our-Projects-Picture">
                <img src="/images/project-1.jpg" alt=""/>
            </div>
            <div class="Our-Projects-Description">
                <p class="Our-Projects-Text-1">Выполнение работ по иготовлению испытательного стенда для проверки
                    выпускаемой продукции</p>

                <p class="Our-Projects-Text-2">
                    Монтаж технологического оборудования и металлических конструкций
                </p>
            </div>
        </div>
        <div class="Our-Projects-Part">
            <div class="Our-Projects-Picture">
                <img src="/images/project-2.jpg" alt=""/>
            </div>
            <div class="Our-Projects-Description">
                <p class="Our-Projects-Text-1">Теплоэлектростанция с ORC модулем</p>

                <p class="Our-Projects-Text-2">
                    Сборка и монтаж технологического оборудования теплоэлектростанции включая ORC турбогенератор и
                    контур диатермического масла
                </p>
            </div>
        </div>
        <div class="Our-Projects-Part">
            <div class="Our-Projects-Picture">
                <img src="/images/project-3.jpg" alt=""/>
            </div>
            <div class="Our-Projects-Description">
                <p class="Our-Projects-Text-1">Реконструкция НТКР. Строительство второй линии для переработки
                    попутного нефтяного газа</p>

                <p class="Our-Projects-Text-2">
                    Монтаж технологического оборудования и металлических конструкций
                </p>
            </div>
        </div>
        <div class="Our-Projects-Part">
            <div class="Our-Projects-Picture">
                <img src="/images/project-4.jpg" alt=""/>
            </div>
            <div class="Our-Projects-Description">
                <p class="Our-Projects-Text-1">Локальная установка по нитрализации и очистке сточных вод с
                    внедрением биохимической очистки и доочистки</p>

                <p class="Our-Projects-Text-2">
                    Монтаж технологического оборудования, трубопроводов и металлических конструкций
                </p>
            </div>
        </div>
        <div class="Our-Projects-Part">
            <div class="Our-Projects-Picture">
                <img src="/images/project-5.jpg" alt=""/>
            </div>
            <div class="Our-Projects-Description">
                <p class="Our-Projects-Text-1">Комплекс переработки нефтяных остатков II пусковой этап. Установка
                    гидроочистки с блоком производства водорода</p>

                <p class="Our-Projects-Text-2">
                    Монтаж технологического оборудования, трубопроводов и металлических конструкций
                </p>
            </div>
        </div>
        <div class="Our-Projects-Part">
            <div class="Our-Projects-Picture">
                <img src="/images/project-6.jpg" alt=""/>
            </div>
            <div class="Our-Projects-Description">
                <p class="Our-Projects-Text-1">Линия по производству гипсового вяжущего</p>

                <p class="Our-Projects-Text-2">
                    Монтаж технологического оборудования и металлических конструкций
                </p>
            </div>
        </div>
    </div>
</div>
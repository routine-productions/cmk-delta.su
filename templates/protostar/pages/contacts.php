<div class="Contacts" id="contacts">
    <iframe class="Map" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"
    <iframe src="https://www.google.com/maps/embed?pb=!1m14!1m8!1m3!1d2118.4905463025693!2d56.1399496!3d57.9286995!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x43e8be3c74d15a9f%3A0x6c542354a33fe9c8!2z0KHQnNCaICLQlNCV0JvQrNCi0JAiLCDQntCe0J4!5e0!3m2!1sru!2sru!4v1428438310995"></iframe>
    <div class="Contacts-Text">
        <p class="Contacts-Text-Head">контактная информация</p>

        <div class="Contacts-Info">
            <p>Почтовый адрес:</p><span>614065, г. Пермь, ул. Промышленная, 101</span>
        </div>
        <br>

        <div class="Contacts-Info">
            <p>Юридический адрес:</p><span>614065, г. Пермь, ш.Космонавтов, 304</span><br>
        </div>
        <div class="Contacts-Info">
            <p>Тел./факс:</p><span>(342) 256-56-93</span><br>
        </div>
        <div class="Contacts-Info">
            <p>e–mail:</p><span><a href="mailto:office@cmkdelta.ru">office@cmkdelta.ru</a></span>
        </div>

        <p class="Contacts-Text-Head-2">Руководство Компании</p>

        <div class="Leader-1">
            <p class="Leader-Head">ЗЕЛЕНКИН СЕРГЕЙ АЛЕКСАНДРОВИЧ</p>

            <p class="Leader-Post">Генеральный директор</p>

            <p class="Email">
                <a href="mailto:s.zelenkin@cmkdelta.ru">s.zelenkin@cmkdelta.ru</a>
            </p>
        </div>
        <div class="Leader">
            <p class="Leader-Head">ВЕСЕЛИЦКИЙ АЛЕКСАНДР ВЯЧЕСЛАВОВИЧ</p>

            <p class="Leader-Post">Первый заместитель генерального директора, главный инженер</p>

            <p class="Email">
                <a href="mailto:a.veselitskiy@cmkdelta.ru">a.veselitskiy@cmkdelta.ru</a>
            </p>
        </div>
        <div class="Leader">
            <p class="Leader-Head">КОРОЛЁВА ЛЮБОВЬ ВАСИЛЬЕВНА</p>

            <p class="Leader-Post">Заместитель генерального директора, главный бухгалтер</p>

            <p class="Email">
                <a href="mailto:l.koroleva@cmkdelta.ru">l.koroleva@cmkdelta.ru</a>
            </p>
        </div>
    </div>
</div>
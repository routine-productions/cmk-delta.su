<div class="Our-Clients-Wrapper" id="recommendations">
    <div class="Our-Clients">
        <p class="Our-Clients-Head">
            Разрешительная документация
        </p>

        <div class="Client">

            <a data-lightbox="group:clients" href="/images/docs-4.png">
                <img src="/images/docs-4.png" alt="">
            </a>
            <a data-lightbox="group:clients" href="/images/docs-5.jpg">
                <img src="/images/docs-5.jpg" alt="">
            </a>
            <a data-lightbox="group:clients" href="/images/docs-6.jpg">
                <img src="/images/docs-6.jpg" alt="">
            </a>
            <a data-lightbox="group:clients" href="/images/docs-7.jpg">
                <img src="/images/docs-7.jpg" alt="">
            </a>
        </div>

        <br>
        <p class="Our-Clients-Head" style="margin-top: 40px;">
            Благодарственные письма
        </p>

        <div class="Client">
            <a data-lightbox="group:clients" href="/images/docs-1.jpg">
                <img src="/images/docs-1.jpg" alt="">
            </a>
            <a data-lightbox="group:clients" href="/images/docs-2.jpg">
                <img src="/images/docs-2.jpg" alt="">
            </a>
            <a data-lightbox="group:clients" href="/images/docs-3.jpg">
                <img src="/images/docs-3.jpg" alt="">
            </a>
        </div>
    </div>
</div>
(function ($) {
    $(document).ready(function () {

        $(".Order").click(function () {
            $(".Wrapper-Order-Block").fadeIn();
        });

        $(".Layout").click(function () {
            $(".Wrapper-Order-Block").fadeOut();
        });


        // Menu Panel Elements Show/Hide
        $(window).scroll(function () {
            if ($(window).scrollTop() > 496) {
                $('.Head-Menu .Menu-Logo').css('left', '55px');
                $('.Head-Menu .Menu-Phone').css('right', '55px');
                $('.Head-Menu').css('background-color', 'rgba(90, 159, 209, 1)');
            } else {
                $('.Head-Menu .Menu-Logo').css('left', '-220px');
                $('.Head-Menu .Menu-Phone').css('right', '-220px');
                $('.Head-Menu').css('background-color', 'transparent');
            }
        });


        // Fallen Menu
        $('.Head-Menu ul > li').hover(function(){
            $(this).find('.nav-child').css({'opacity':1,'height': $(this).find('.nav-child li').length * $(this).find('.nav-child li').outerHeight() + 20,'padding-top':'10px','padding-bottom':'10px'});
        },function(){
            $(this).find('.nav-child').css({'opacity':0,'height':'0','padding-top':'0px','padding-bottom':'0px'});
        });



////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Send Mail By Ajax
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

        $('.Head-Form .Call-Me').click(function () {

            //Get Data
            var Phone = $('.Head-Form [name=phone]').val();


            // Set Content
            var Query = 'phone=' + Phone;

            // Validate Email

            if (Phone.length > 0) {
                $.ajax({
                    url: '/actions/connect.php',
                    type: 'POST',
                    data: Query,
                    success: function (Data) {
                        $('.Registration-Success, .Registration-Error').remove();

                        $('.Head-Form .Call-Me').after(Data);

                        $(".Wrapper-Order-Block").delay(2000).fadeOut();

                    },
                    error: function () {
                        $('.Registration-Success, .Registration-Error').remove();

                        $('.Head-Form .Call-Me').after('<div class="Registration-Error">Ошибка, попробуйте позже.</div>');
                    }
                });
            } else {
                $('.Registration-Success, .Registration-Error').remove();
                $('.Head-Form .Call-Me').after('<div class="Registration-Error">Введите телефон!</div>');
            }

            return false;
        });

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        $(window).resize(function () {
            $('.Red-Kran-Logo').css('marginLeft', '-' + ($('.Red-Kran-Logo').width() / 2 + 20) + 'px');
            $('.Red-Kran-Logo').animate({'top': '-' + ($('.Red-Kran-Logo').height() - $('.Red-Kran').height()) + 'px'}, 2000);
        });

    });


    $(window).load(function () {
        $('.Red-Kran-Logo').css('marginLeft', '-' + ($('.Red-Kran-Logo').width() / 2 + 20) + 'px');
        $('.Red-Kran-Logo').animate({'top': '-' + ($('.Red-Kran-Logo').height() - $('.Red-Kran').height()) + 'px'}, 2000);
    });

})(jQuery);
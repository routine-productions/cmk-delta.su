<?php
$URI = array_values(array_filter(explode('/', $_SERVER['REQUEST_URI'])));
?>
<html>
<head>
    <title>СМК "Дельта"</title>
    <jdoc:include type="head"/>
    <link rel="icon" type="image/png" href="/favicon.png"/>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

    <script src="/templates/protostar/js/script.js"></script>
    <link rel="stylesheet" href="/templates/protostar/css/style.css"/>
    <link rel="stylesheet" href="/templates/protostar/css/media.css"/>
    <link href='http://fonts.googleapis.com/css?family=Exo+2:400,300,500,600&subset=cyrillic,latin'
        rel='stylesheet'
        type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans' rel='stylesheet' type='text/css'>
</head>
<body class="Page-<?= $URI[0] ?>">
<div class="Wrapper">
    <div class="Header">
        <div class="Head-Menu">
            <a href="/">
                <img src="/images/menu-logo.png" alt="Cmk Delta" class="Menu-Logo"/>
            </a>
            <jdoc:include type="modules" name="menu"/>
            <p class="Menu-Phone">
                <img src="/ico/phone.png" alt=""/>
                (342) 256-56-93
            </p>
        </div>


        <div class="Head-Menu-Sub" id="main">
            <div class="Head-Delta-Logo">
                <a href="/">
                    <img src="/images/red-logo-kran.png" alt="" class="Red-Kran-Logo" style=""/>
                </a>

                <img src="/images/red-logo.png" class="Red-Kran"/>
            </div>
            <div class="Head-Center-Text">
                <p>монтаж технологических инженерных систем<br> в химии и нефтехимии</p>
            </div>
            <div class="Head-Phone">
                <p class="Phone-Number">
                    <img src="/ico/phone.png" alt=""/>
                    (342) 256-56-93
                </p>
                <button class="Order">Заказать обратный звонок</button>
            </div>
        </div>
    </div>

    <!-- Start Content -->
    <div class="Before-Component">
        <?php if ($this->countModules('before_component')) : ?>
            <hr/>
            <span class="Our-Projects-Slider">
                <b>Наши проекты</b>
        </span>

            <jdoc:include type="modules" name="before_component"/>
        <?php endif; ?>
    </div>

    <div class="Component">
        <jdoc:include type="component"/>
    </div>

    <div class="Html-Content">
        <?php
        $File = dirname(__FILE__) . '/pages/' . $URI[0] . '.php';

        if (!empty($URI[1])) {
            $File = dirname(__FILE__) . '/pages/' . $URI[0] . '.' . $URI[1] . '.php';
        }

        if (file_exists($File)) {
            require_once $File;
        } else if (empty($URI[0])) {
            require_once 'pages/home.php';
        }

        ?>
    </div>

    <?php
    if (empty($URI[0])) {
        require_once 'pages/about.php';
        require_once 'pages/about.activities.php';
    }
    ?>


    <?php if ($this->countModules('clients')) : ?>
        <div class="After-Component Our-Clients">
            <hr/>
            <span class="Our-Clients-Head">
                Наши клиенты
            </span>
            <jdoc:include type="modules" name="clients"/>
        </div>
    <?php endif; ?>

    <?php if ($this->countModules('partners')) : ?>
        <div class="After-Component Our-Clients">
            <hr/>
            <span class="Our-Clients-Head">
                Наши парнеры
            </span>
            <jdoc:include type="modules" name="partners"/>
        </div>
    <?php endif; ?>


    <?php if ($this->countModules('news')) : ?>
        <div class="After-Component Our-Clients">
            <hr/>
            <span class="Our-Clients-Head">
                Новости
            </span>
            <jdoc:include type="modules" name="news"/>
        </div>
    <?php endif; ?>

    <div class="Before-Component">
        <?php if ($this->countModules('slider_2')) : ?>
            <hr/>
            <span class="Our-Projects-Slider">
                <b>Наши проекты</b>
        </span>

            <jdoc:include type="modules" name="slider_2"/>
        <?php endif; ?>
    </div>
    <!-- End Content -->


    <?php
    if (empty($URI[0])) {
        require_once 'pages/projects.php';
        require_once 'pages/contacts.php';
    }
    ?>





    <div class="Footer">
        <img src="/images/one-builder.png" alt=""/>
        <div class="Head-Delta-Logo">
            <!--            <p class="Delta-Logo-Text-1">ДЕЛЬТА</p>-->
            <!--            <p class="Delta-Logo-Text-2">СТРОИТЕЛЬНО-МОНТАЖНАЯ КОМПАНИЯ</p>-->
            <img src="/images/logo-bottom.png"/>
        </div>
        <p class="Footer-Text">Copyright © 2015 | Общество с ограниченной ответственностью "СМК "ДЕЛЬТА"</p>
    </div>
</div>


<div class="Wrapper-Order-Block">
    <div class="Layout"></div>
    <div class="Order-Block">
        <p class="Order-Block-Head">Заказать обратный звонок</p>

        <p class="Your-Phone">Ваш телефон:</p>

        <form class="Head-Form" action="">
            <input type="text" name="phone">
            <button class="Call-Me">Перезвоните мне</button>
        </form>
    </div>
</div>
</body>
</html>
